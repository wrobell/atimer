Changelog
=========
2025-03-02, ver. 1.3.0
----------------------
- edge of interval can be moved by a time value with timer `shift`
  parameter

2024-07-31, ver. 1.2.0
----------------------
- require Python 3.12 or later
- code cleanup

2023-07-21, ver. 1.1.0
----------------------
- update to Cython 3.0.0
- documentation improvements

2022-01-18, ver. 1.0.1
----------------------
- improved type annotations

2021-06-30, ver. 1.0.0
----------------------
- documentation updates

2020-05-14, ver. 0.3.3
----------------------
- documentation updates

2019-05-01, ver. 0.3.2
----------------------
- documentation updates

2019-03-05, ver. 0.3.1
----------------------
- minor code cleanup

2019-02-01, ver. 0.3.0
----------------------
- synchronize timer with real time clock at start
- use boot time clock for the timer to track number of expirations when
  a machine is suspended

2018-12-07, ver. 0.2.2
----------------------
- build and installation fixes

2018-12-07, ver. 0.2.1
----------------------
- documentation fixes

2018-12-07, ver. 0.2.0
----------------------
- switched to used cython instead of cffi

2017-02-24, ver. 0.1.0
----------------------
- initial release

.. vim: sw=4:et:ai
