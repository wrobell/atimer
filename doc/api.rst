API
===
The API consist of *Timer* class, which object represents asynchronous
timer.

.. autoclass:: atimer.Timer
    :members:

.. vim: sw=4:et:ai
